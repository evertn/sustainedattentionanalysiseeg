%{ 
    Aim of this file is to find bad channels for every second.
    You need:
        data_store
        sum_students
        window
        sampling
        duration_in_seconds_over_subjects
        nr_of_electordes
    to run this file
%}

%% Configuration
datapoint_counter           =   1;
isOk                        =   ones(sum_students,nr_of_electordes,duration_in_seconds_over_subjects); % So in this experiment (15,14,5000)
channels                    =   nr_of_electordes;
just_counting_bad_seconds   =   0;
bool_for_checking_if_sec_is_ok  =   1;

%%
for i=1:sum_students
    for ii=1:channels
        for iii=1:seconds
            if (max(abs(flt_data{1,i}.trial{1,1}(ii,datapoint_counter:datapoint_counter+128)))>150) %using preprocessed data (flt_data)
                bool_for_checking_if_sec_is_ok=0;
            end
            datapoint_counter=datapoint_counter+128;
            if (bool_for_checking_if_sec_is_ok==0)
                isOk(i,ii,iii)=0;
                just_counting_bad_seconds=just_counting_bad_seconds+1;
            end
            bool_for_checking_if_sec_is_ok=1;
        end
        datapoint_counter=1;
    end
    datapoint_counter=1;
end