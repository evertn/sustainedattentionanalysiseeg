%{
    Reads "1katseisik.CSV" to "15katseisik.CSV files into Matlab

%}

sum_students    =   15;
data_store      =   cell(1,sum_students);
for i=1:sum_students
    filename = strcat(num2str(i),'katseisik.CSV'); 
    M = csvread(filename);
    data_store(i)={M( : ,3:16 )'}; % Take only EEG data and exclude gyroscope data
    clearvars filename;
    clearvars M;
end
