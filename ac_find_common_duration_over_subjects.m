%{ 
    Finding experiment duration (common amount of seconds of all subjects)
    You need:
        data_store
        sum_students
        window
        sampling
    to run this file
%}

%% Configuration
a=ones(1,sum_students);

%% 
for ii=1:sum_students
	a(1,ii)=length(window/2:1:size(data_store{ii},2)/sampling);
end

duration_in_seconds_over_subjects=min(a);
clearvars a;
