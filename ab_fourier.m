%{
    Aim of this file is to perform Fast Fourier Transform. 
    
    This file needs:
            data_store <1x15>   - raw data of all subjects
            flt_store <1x15>    - ft_preprocessed data over all subjects
            sum_students        - number of participants
            sampling            - sampling rate of EEG machine
%}

window              =   1;      % sec
sampling            =   128;    % sampling rate of EEG device
FFTresults          =   cell(1,sum_students);

for ii=1:sum_students
    
    %% FFT Configuration
    cfg             =   [];
    cfg.output      =   'pow';
    cfg.channel     =   'all'; 
    cfg.method      =   'mtmconvol';      % implements multitaper time-frequency transformation based on multiplication in the frequency domain.
    cfg.taper       =   'hanning';
    cfg.foi         =   2:1:45;           % frequencies of interest
    cfg.t_ftimwin   =   ones(length(cfg.foi),1) .* window;            % vector 1 x numfoi- length of time window -in seconds
    cfg.toi         =   window/2:1:size(data_store{ii},2)/sampling;   % the times on which the analysis windows should be centered (in seconds)
    
    FFTresults{ii}  =   ft_freqanalysis(cfg, flt_data{ii});
    
end

% ft_singleplotTFR(cfg, FFTresults{11}); 
