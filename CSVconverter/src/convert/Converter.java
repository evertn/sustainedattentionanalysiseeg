package convert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Converter {

	public static void main(String[] args) throws FileNotFoundException {
		// Program goes through files 1isik.CSV to 15isik.CSV and eliminates headers.
		int sum_students = 15;
		String in = "isik.CSV"; 
		String out = "katseisik.CSV";
		for (int i = 1; i <= sum_students; i++) {
			File filenameIn = new java.io.File(i + in);
			File filenameOut = new java.io.File(i + out);
			PrintWriter pw = new java.io.PrintWriter(filenameOut);
			Scanner sc = new java.util.Scanner(filenameIn);
			if (sc.hasNextLine()) {
				sc.nextLine(); // First line is header what is not needed.
			}
			while (sc.hasNextLine()) {
				String row = sc.nextLine();
				pw.println(row);
			}
		}
	}

}
