
%{ 
    Aim of this file is to avarage data over minutes and also plot the
    results
    You need:
        avg_store_over_channels(15,5000); % 15 students, 5000 seconds
        sum_students
        powspctrm_size
    to run this file

%}
%% Configuration
avg_minutes=zeros(15,83); % sum_students, duration of the experiment in minutes

%%
avg_store_over_channels(avg_store_over_channels==0)=NaN; % zeros to NaN's - to take nanmean later on

for ii=1:sum_students
    x=1;
    for j=1:60:4980, % because 4980sec=83min
        avg_minutes(ii,x)=nanmean(avg_store_over_channels(ii,j:j+60));
        x=x+1;
    end
    %clearvars ii;
    clearvars j;
    clearvars k;
    clearvars x;
end
clearvars powspctrm_size;

% Normalize data
Z=zscore(avg_minutes'); %normalize
Z=Z';
figure;
hold on
for kk=1:sum_students
        plot(Z(kk,1:83),'Color',rand(1,3));
end

main_avg=zeros(1,83); % 83 minutes of data
for ccc=1:83
    for cc=1:sum_students
        if (cc~=3)
        if (cc~=4)
            main_avg(ccc)=main_avg(ccc)+Z(cc,ccc);
        end
        end
    end
    main_avg(ccc)=(main_avg(ccc)/(sum_students-2));
end


plot(main_avg,'LineWidth',3);

question_minutes=[2,6,37,43,57,58,61,63,65,82,83];
for jj=1:length(question_minutes)
    plot([question_minutes(jj),question_minutes(jj)],[-3,6],'--r');
end

aswer_minutes=[5,39,44,57.5, 59, 62,64, 66,5];
for jj=1:length(aswer_minutes)
    plot([aswer_minutes(jj),aswer_minutes(jj)],[-3,6],'--g'); % 
end



hold off
