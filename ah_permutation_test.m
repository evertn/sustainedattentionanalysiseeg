
%{ 
    Performing permutation test to validate if attenion increases after
    questions are being asked.
    You need:
        Z(14,83);  %normalized data

%}

%% Variables
questions_before=[];
questions_after=[];
questions_all=[];
count =10000;

%% Adding data

for ii=1:14,
    if (ii~=3)
    if (ii~=4)
        questions_before(end+1)     =   Z(ii, 36);
        questions_before(end+1)     =   Z(ii, 42);
        questions_before(end+1)     =   Z(ii, 56);
        
        questions_after(end+1)      =   Z(ii, 40);
        questions_after(end+1)      =   Z(ii, 45);
        questions_after(end+1)      =   Z(ii, 67);
    end
    end
    
end
questions_all   =   [questions_before(1:36);questions_after(1:36)];
after=mean(questions_after(1:36));
before=mean(questions_before(1:36));

MyResult=mean(questions_after(1:36))-mean(questions_before(1:36));

questions_all = [];
for kk=1:36,
    questions_all(end+1) = questions_before(kk);
end

for kk=1:36,
    questions_all(end+1) = questions_after(kk);
end


results=zeros(count,1);
for ii=1:count
  temp=questions_all(randperm(72));
  results(ii)=mean(temp(1:36))-mean(temp(37:72));
end

hist(results,30);
sum(results>MyResult)/count
hold on
%sum(results>MyResult)/count
plot([MyResult,MyResult],ylim,'r--','LineWidth',2);
hold off
