
%{ 
    Aim of this file is to avarage high/low ratio over channels for every
    participant excluding noisy channels every second.
    You need:
        avg_store(15,14,5000)
        isOK
        powspctrm_size
        nr_of_electordes
    to run this file

%}
%% Configuration

avg_store_over_channels             =    zeros(sum_students,duration_in_seconds_over_subjects);
tmp_variable_for_nr_of_channels     =    14; %same number as nr_of_electordes

%%
for ii=1:sum_students
    for j=1:powspctrm_size,
       for k=1:nr_of_electordes,
            if (isOk(ii,k,j)==0)
                tmp_variable_for_nr_of_channels= tmp_variable_for_nr_of_channels-1;              
            else
                avg_store_over_channels(ii,j)=avg_store_over_channels(ii,j)+avg_store(ii,k,j);
            end
        end
        if (tmp_variable_for_nr_of_channels~=0)
            avg_store_over_channels(ii,j)=(avg_store_over_channels(ii,j)/tmp_variable_for_nr_of_channels);
        end
        tmp_variable_for_nr_of_channels=14;        
    end
    clearvars j;
    clearvars k;
end
clearvars powspctrm_size;

