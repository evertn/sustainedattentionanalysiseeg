These scripts need Matlab (http://www.mathworks.se/products/matlab/) and it also uses plugin called 
FielTrip (http://fieldtrip.fcdonders.nl/).
MATLAB is a high-level language and interactive environment for numerical computation, visualization, and programming.
FieldTrip is the Matlab software toolbox for MEG and EEG analysis.

Scripts were written to analyse Emotiv EPOC EEG data.
Those scripts wew written to examine students' attention during lecture to identify if attention increases 
if questions are being asked during lecture. 16 students watched video lecture and at the same time 
their brain activity was measured with Emotiv EPOC EEG device.
Those scripts find change of attention in time (ratio of high/low frequency) and also perform statistical 
tests to identify if attention increases due to question asking.

To run scripts it needs Emotiv Epoc data (emotiv.com).
Emotiv Epoc Research Edition has software called TestBench which allows to record EEG data. 

Remove unnecessary headers from EEG .CSV files:
00) CSVconverter - Eclipse project
Import EEG data from CSV files to Matlab:
01) CSV_to_Matlab.m

In Matlab scripts must run in that order:
1) aa_preprocessing.m
2) ab_fourier.m
3) ac_find_common_duration_over_subjects.m
4) ad_generate_matrix_to_identify_noisy_channels_for_every_second.m
5) ae_high_low_ratio_of_every_sec.m
6) af_avg_high_low_ratio_over_channels_exclude_noise.m
7) ag_avg_ratio_minutes_plots.m
8) ah_permutation_test.m

Explanation of files: 
1) aa_preprocessing.m
	Producing filtered data (It performs bandbass filter).
2) ab_fourier.m
	Performing Fast Fourier Transform.
3) ac_find_common_duration_over_subjects.m
	Finding experiment duration (common amount of seconds of all subjects).
4) ad_generate_matrix_to_identify_noisy_channels_for_every_second.m
	Finding bad channels for every second.
5) ae_high_low_ratio_of_every_sec.m
	Finding ratio of high/low frequency of every second. High freq.= 25-45 Hz. Low freq.= 5-16 Hz.
6) af_avg_high_low_ratio_over_channels_exclude_noise.m
	Taking average of channels for every participant and also eliminating noisy channels every second.
7) ag_avg_ratio_minutes_plots.m
	Averaging over minutes and plotting the results.
8) ah_permutation_test.m
 	Performing permutation test to validate if attenion increases after questions are being asked.


