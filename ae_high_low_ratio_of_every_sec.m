
%{ 
    Aim of this file is to find ratio of high/low frequency of every
    second. High freq.= 25-45 Hz. Low freq.= 5-14 Hz
    You need:
        FFTresults
        sum_students
        duration_in_seconds_over_subjects
    to run this file


%}
%% Configuration
avg_store       =   ones(sum_students,14,duration_in_seconds_over_subjects);
powspctrm_size  =   duration_in_seconds_over_subjects;

%%
for ii=1:sum_students

    high        =   zeros(1,powspctrm_size);
    low         =   zeros(1,powspctrm_size);
    avg         =   zeros(1,powspctrm_size);

    for j=1:powspctrm_size,
        for k=1:14,         % For loop over electrodes
            for i=1:45,     % For loop over frequencies 1-45 Hx
                if i>24     % If over 25 Hz then high
                        high(j)=high(j)+(FFTresults{ii}.powspctrm(k,i,j)); 
                elseif i>4 && i<15  % If 5-14 Hz the low 
                        low(j)=low(j)+(FFTresults{ii}.powspctrm(k,i,j));
                else
                end
            end
            avg_store(ii,k,j)=high(j)/low(j);
            high(j) =    0;
            low(j)  =    0;
            
        end
        
    end
    clearvars low;
    clearvars high;
    clearvars avg;
    clearvars i;
    clearvars j;
    clearvars k;
end


