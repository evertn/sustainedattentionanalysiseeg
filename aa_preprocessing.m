%{
    Aim of this file is to produce filtered data 
    (It performs bandbass filter)
    
    This script needs:
            data_store <1x15>  - raw data of all subjects

%}
nr_of_electordes    =   14;     % Nr of electrodes of EEG device
sum_students        =   15;     % Nr of participants
flt_data            =   cell(1,sum_students);

for ii=1:sum_students
 %% Configuration
    sampling        =   128;
    ch_data         =   1:14;
    electrodes      =   {'AF3', 'F7', 'F3', 'FC5', 'T7', 'P7', 'O1', 'O2', 'P8', 'T8', 'FC6', 'F4', 'F8', 'AF4'};
    window          =   1; %sec
    time            =   0:(1/sampling):size(data_store{ii},2)/sampling;
 %% Build data structure
    flt_data{ii}.label      =   electrodes;
    flt_data{ii}.fsample    =   sampling;           % sampling frequency in Hz, single number
    flt_data{ii}.trial      =   {data_store{ii}};   % cell-array containing a data matrix for each trial (1 X Ntrial), each data matrix is    Nchan X Nsamples 
    flt_data{ii}.time       =   {time};             % cell-array containing a time axis for each trial (1 X Ntrial), each time axis is a 1 X Nsamples vector 

    
 %% Filter and remove artifacts
    cfg             =   [];
    cfg.channel     =   'all';
    cfg.bpfilter    =   'yes';        %band-pass filer (passes frequencies within a certain range)
    cfg.bpfreq      =   [1 45];       %band-pass frequency range, specified as [low high] in Hz
    cfg.bpfiltdir   =   'twopass';
    
    flt_data{ii}    =   ft_preprocessing(cfg, flt_data{ii});
end

%ft_databrowser(cfg, flt_data{1}); 